io.stdout:setvbuf('no')
if arg[#arg] == "-debug" then require("mobdebug").start() end

love.graphics.setDefaultFilter("nearest")

-- Collision detection function
-- FROM https : //love2d.org/wiki/BoundingBox.lua
function CheckCollision(x1, y1, w1, h1, x2, y2, w2, h2)
  return x1 < x2 + w2 and
  x2 < x1 + w1 and
  y1 < y2 + h2 and
  y2 < y1 + h1
end

-- Images loading
local imgTiles = {}
imgTiles["1"] = love.graphics.newImage("images/tile1.png")
imgTiles["2"] = love.graphics.newImage("images/tile2.png")
imgTiles["3"] = love.graphics.newImage("images/tile3.png")
imgTiles["4"] = love.graphics.newImage("images/tile4.png")
imgTiles["5"] = love.graphics.newImage("images/tile5.png")
imgTiles["="] = love.graphics.newImage("images/tile=.png")
imgTiles["["] = love.graphics.newImage("images/tile[.png")
imgTiles["]"] = love.graphics.newImage("images/tile].png")
imgTiles["H"] = love.graphics.newImage("images/tileH.png")
imgTiles["#"] = love.graphics.newImage("images/tile#.png")
imgTiles["g"] = love.graphics.newImage("images/tileg.png")

-- Map and levels
local map = {}
local level = {}
local lstSprites = {}
local player = nil
local currentLevel = 1
local levels = {
  "The beginning",
  "Welcome in hell"
}

local TILESIZE = 16

-- Globals
local bJumpReady

function ChargeNiveau(pNum)
  lstSprites = {}
  map = {}
  local filename = "levels/level"..tostring(pNum)..".txt"
  for line in io.lines(filename) do 
    map[#map + 1] = line
  end
  -- Look for the player in the map
  level = {}
  level.playerStart = {}
  level.playerStart.col = 0
  level.playerStart.lig = 0
  level.coins = 0
  for l=1,#map do
    for c=1,#map[1] do
      local char = string.sub(map[l],c,c)
      if char == "P" then
        level.playerStart.col = c
        level.playerStart.lig = l
        player = CreatePlayer(c,l)
      elseif char == "c" then
        CreateCoin(c,l)
        level.coins = level.coins + 1
      elseif char == "D" then
        CreateDoor(c,l)
      end
    end
  end
end

function NextLevel()
  currentLevel = currentLevel + 1
  if currentLevel > #levels then
    currentLevel = 1
  end
  ChargeNiveau(currentLevel)
end

function isSolid(pID)
  if pID == "0" then return false end
  if pID == "1" then return true end
  if pID == "5" then return true end
  if pID == "4" then return true end
  if pID == "=" then return true end
  if pID == "[" then return true end
  if pID == "]" then return true end
  return false
end

function isJumpThrough(pID)
  if pID == "g" then return true end
  return false
end

function isLadder(pID)
  if pID == "H" then return true end
  if pID == "#" then return true end
  return false
end

function CreateSprite(pType, pX, pY)
  local mySprite = {}

  mySprite.x = pX
  mySprite.y = pY
  mySprite.vx = 0
  mySprite.vy = 0
  mySprite.gravity = 0
  mySprite.isJumping = false
  mySprite.type = pType
  mySprite.standing = false
  mySprite.flip = false
  mySprite.open = false

  mySprite.currentAnimation = ""
  mySprite.frame = 0
  mySprite.animationSpeed = 1/8
  mySprite.animationTimer = mySprite.animationSpeed
  mySprite.animations = {}
  mySprite.images = {}

  mySprite.AddImages = function(psDir, plstImage)
    for k,v in pairs(plstImage) do
      local fileName = psDir.."/"..v..".png"
      mySprite.images[v] = love.graphics.newImage(fileName)
    end
  end

  mySprite.AddAnimation = function(psDir, psName, plstImages)
    mySprite.AddImages(psDir, plstImages)
    mySprite.animations[psName] = plstImages
  end

  mySprite.PlayAnimation = function(psName)
    if mySprite.currentAnimation ~= psName then
      mySprite.currentAnimation = psName
      mySprite.frame = 1
    end
  end

  table.insert(lstSprites, mySprite)

  return mySprite
end

function CreatePlayer(pCol, pLig)
  local myPlayer = CreateSprite("player", (pCol - 1) * TILESIZE, (pLig - 1) * TILESIZE)
  myPlayer.gravity = 500
  myPlayer.AddAnimation("images/player", "idle", {"idle1", "idle2", "idle3", "idle4"})
  myPlayer.AddAnimation("images/player", "run", {"run1", "run2", "run3", "run4", "run5", "run6", "run7", "run8", "run9", "run10"})
  myPlayer.AddAnimation("images/player", "climb", {"climb1", "climb2"})
  myPlayer.AddAnimation("images/player","climb_idle", {"climb1"})
  myPlayer.PlayAnimation("idle")
  bJumpReady = true
  return myPlayer
end

function CreateCoin(pCol, pLig)
  local myCoin = CreateSprite("coin", (pCol - 1) * TILESIZE, (pLig - 1) * TILESIZE)
  myCoin.AddAnimation("images/coin", "coin", {"coin1", "coin2", "coin3", "coin4"})
  myCoin.PlayAnimation("coin")
end

function CreateDoor(pCol, pLig)
  local myDoor = CreateSprite("door", (pCol-1)*TILESIZE, (pLig-1)*TILESIZE)
  myDoor.AddAnimation("images/door", "open", {"door-open"})
  myDoor.AddAnimation("images/door", "close", {"door-close"})
  myDoor.PlayAnimation("close")
end

function InitGame(pNiveau)
  ChargeNiveau(pNiveau)
end

function love.load()
  love.window.setTitle("Mini platformer")
  InitGame(1)
end

function AlignOnLine(pSprite)
  local lig = math.floor((pSprite.y + TILESIZE/2) / TILESIZE) + 1
  pSprite.y = (lig-1)*TILESIZE
end

function AlignOnColumn(pSprite)
  local col = math.floor((pSprite.x + TILESIZE/2) / TILESIZE) + 1
  pSprite.x = (col-1)*TILESIZE
end

function updatePlayer(pPlayer, dt)
  -- Locals for Physics
  local accel = 350
  local friction = 120
  local maxSpeed = 100
  local jumpSpeed = -190
  -- Tile under the player
  local idUnder = getTileAt(pPlayer.x + TILESIZE/2, pPlayer.y + TILESIZE)
  local idOverlap = getTileAt(pPlayer.x + TILESIZE/2, pPlayer.y + TILESIZE-1)
  -- Stop Jump?
  if pPlayer.isJumping and (CollideBelow(pPlayer) or isLadder(idUnder)) then
    pPlayer.isJumping = false
    pPlayer.standing = true
    AlignOnLine(pPlayer)
  end
  -- Friction
  if pPlayer.vx > 0 then
    pPlayer.vx = pPlayer.vx - friction * dt
    if pPlayer.vx < 0 then pPlayer.vx = 0 end
  end
  if pPlayer.vx < 0 then
    pPlayer.vx = pPlayer.vx + friction * dt
    if pPlayer.vx > 0 then pPlayer.vx = 0 end
  end
  local newAnimation = "idle"
  -- Keyboard
  if love.keyboard.isDown("right") then
    pPlayer.vx = pPlayer.vx + accel*dt
    if pPlayer.vx > maxSpeed then pPlayer.vx = maxSpeed end
    pPlayer.flip = false
    newAnimation = "run"
  end
  if love.keyboard.isDown("left") then
    pPlayer.vx = pPlayer.vx - accel*dt
    if pPlayer.vx < -maxSpeed then pPlayer.vx = -maxSpeed end
    pPlayer.flip = true
    newAnimation = "run"
  end
  -- Check if the player overlap a ladder
  local isOnLadder = isLadder(idUnder) or isLadder(idOverlap)
  if isLadder(idOverlap) == false and isLadder(idUnder) then
    pPlayer.standing = true
  end
  -- Jump
  if love.keyboard.isDown("up") and pPlayer.standing and bJumpReady and isLadder(idOverlap) == false then
    pPlayer.isJumping = true
    pPlayer.gravity = 500
    pPlayer.vy = jumpSpeed
    pPlayer.standing = false
    bJumpReady = false
  end
  -- Climb
  if isOnLadder and pPlayer.isJumping == false then
    pPlayer.gravity = 0
    pPlayer.vy = 0
    bJumpReady = false
  end
  if isLadder(idUnder) and isLadder(idOverlap) then
    newAnimation = "climb_idle"
  end
  if love.keyboard.isDown("up") and isOnLadder == true and pPlayer.isJumping == false then
    pPlayer.vy = -50
    newAnimation = "climb"
  end
  if love.keyboard.isDown("down") and isOnLadder == true then
    pPlayer.vy = 50
    newAnimation = "climb"
  end
  -- Not climbing
  if isOnLadder == false and pPlayer.gravity == 0 and pPlayer.isJumping == false then
    pPlayer.gravity = 500
  end
  -- Ready for next jump
  if love.keyboard.isDown("up") == false and bJumpReady == false and pPlayer.standing == true then
    bJumpReady = true
  end
  pPlayer.PlayAnimation(newAnimation)
  -- Move
  pPlayer.x = pPlayer.x + pPlayer.vx * dt
  pPlayer.y = pPlayer.y + pPlayer.vy * dt
end

function getTileAt(pX, pY)
  local col = math.floor(pX / TILESIZE) + 1
  local lig = math.floor(pY / TILESIZE) + 1
  if col>0 and col<=#map[1] and lig>0 and lig<=#map then
    local id = string.sub(map[lig],col,col)
    return id
  end
  return 0
end

function CollideRight(pSprite)
  local id1 = getTileAt(pSprite.x + TILESIZE, pSprite.y + 3)
  local id2 = getTileAt(pSprite.x + TILESIZE, pSprite.y + TILESIZE - 2)
  if isSolid(id1) or isSolid(id2) then return true end
  return false
end

function CollideLeft(pSprite)
  local id1 = getTileAt(pSprite.x-1, pSprite.y + 3)
  local id2 = getTileAt(pSprite.x-1, pSprite.y + TILESIZE - 2)
  if isSolid(id1) or isSolid(id2) then return true end
  return false
end

function CollideBelow(pSprite)
  local id1 = getTileAt(pSprite.x + 1, pSprite.y + TILESIZE)
  local id2 = getTileAt(pSprite.x + TILESIZE-2, pSprite.y + TILESIZE)
  if isSolid(id1) or isSolid(id2) then return true end
  if isJumpThrough(id1) or isJumpThrough(id2) then
    local lig = math.floor((pSprite.y + TILESIZE/2) / TILESIZE) + 1
    local yLine = (lig-1)*TILESIZE
    local distance = pSprite.y - yLine
    if pSprite.vy < 0 then return false end
    if distance >= 0 and distance < 10 then
      return true
    end
  end
  return false
end

function CollideAbove(pSprite)
  local id1 = getTileAt(pSprite.x + 1, pSprite.y-1)
  local id2 = getTileAt(pSprite.x + TILESIZE-2, pSprite.y-1)
  if isSolid(id1) or isSolid(id2) then return true end
  return false
end

function updateSprite(pSprite, dt)
  -- Locals for Collisions
  local oldX = pSprite.x
  local oldY = pSprite.y

  --Door open
  if pSprite.type == "door" and pSprite.open == false then
    if level.coins == 0 then
      pSprite.open = true
      pSprite.PlayAnimation("open")
    end
  end

  -- Specific behavior for the player
  if pSprite.type == "player" then
    updatePlayer(pSprite, dt)
  end

  -- Animation
  if pSprite.currentAnimation ~= "" then
    pSprite.animationTimer = pSprite.animationTimer - dt
    if pSprite.animationTimer <= 0 then
      pSprite.frame = pSprite.frame + 1
      pSprite.animationTimer = pSprite.animationSpeed
      if pSprite.frame > #pSprite.animations[pSprite.currentAnimation] then
        pSprite.frame = 1
      end
    end
  end

  -- Collision detection
  local collide = false
  -- Above
  if pSprite.vy < 0 then
    collide = CollideAbove(pSprite)
    if collide then
      pSprite.vy = 0
      AlignOnLine(pSprite)
    end
  end
  collide = false
  -- Below
  if pSprite.standing or pSprite.vy > 0 then
    collide = CollideBelow(pSprite)
    if collide then
      pSprite.standing = true
      pSprite.vy = 0
      AlignOnLine(pSprite)
    else
      pSprite.standing = false
    end
  end
  collide = false
  -- On the right
  if pSprite.vx > 0 then
    collide = CollideRight(pSprite)
  end
  -- On the left
  if pSprite.vx < 0 then
    collide = CollideLeft(pSprite)
  end
  -- Stop!
  if collide then
    pSprite.vx = 0
    AlignOnColumn(pSprite)
  end
  -- Sprite falling
  if pSprite.standing == false then
    pSprite.vy = pSprite.vy + pSprite.gravity * dt
  end
end

function love.update(dt)  
  for nSprite=#lstSprites,1,-1 do
    local sprite = lstSprites[nSprite]
    updateSprite(sprite, dt)

    -- Check collision with the player
    if sprite.type ~= "player" then
      -- Check coin collision
      if sprite.type == "coin" then
        if CheckCollision(player.x, player.y, TILESIZE, TILESIZE,
          sprite.x + TILESIZE/2, sprite.y + TILESIZE/2, TILESIZE/2, TILESIZE/2) then
          table.remove(lstSprites, nSprite)
          level.coins = level.coins - 1
        end
      elseif sprite.type == "door" and sprite.open then
        if CheckCollision(player.x, player.y, TILESIZE, TILESIZE,
          sprite.x, sprite.y, TILESIZE, TILESIZE) then
          NextLevel()
        end
      end
    end
  end
end

function drawSprite(pSprite)
  local imgName = pSprite.animations[pSprite.currentAnimation][pSprite.frame]
  local img = pSprite.images[imgName]
  local halfw = img:getWidth()  / 2
  local halfh = img:getHeight() / 2
  local flipCoef = 1
  if pSprite.flip then flipCoef = -1 end
  love.graphics.draw(
    img, -- Image
    pSprite.x + halfw, -- horizontal position
    pSprite.y + halfh, -- vertical position
    0, -- rotation (none = 0)
    1 * flipCoef, -- horizontal scale
    1, -- vertical scale (normal size = 1)
    halfw, halfh -- horizontal and vertical offset
  )
end

function love.draw()
  love.graphics.scale(2,2)
  love.graphics.print("Level "..currentLevel..": "..levels[currentLevel], 5,(TILESIZE*18)-3)

  for l=1,#map do
    for c=1,#map[1] do
      local char = string.sub(map[l],c,c)
      if tonumber(char) ~= 0 then
        if imgTiles[char] ~= nil then
          love.graphics.draw(imgTiles[char],(c-1)*TILESIZE,(l-1)*TILESIZE)
        end
      end
    end
  end
  for nSprite=#lstSprites,1,-1 do
    local sprite = lstSprites[nSprite]
    drawSprite(sprite)
  end
end  